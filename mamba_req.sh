#!/bin/bash
# Install the python packages from requirements.txt
# Uses mamba by default and pip if mamba couldn't find package
# Put script in same folder as requirements.txt (or change path here)
# Conda version: just replace mamba by conda

while read requirement; do mamba install --yes $requirement || pip install $requirement; done < requirements.txt
