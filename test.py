import torch
model = torch.nn.Sequential(*[torch.nn.Flatten(),torch.nn.Linear(28*28,512),torch.nn.ReLU(),torch.nn.Linear(512,64),torch.nn.ReLU(),torch.nn.Linear(64,10)])
import image.netmodels.train_model as trainmodel

trainmodel.train(model, 'MNIST', 'cpu', 7)